# Shopify Intern Challenge

- [Challenge description](https://docs.google.com/document/d/1z9LZ_kZBUbg-O2MhZVVSqTmvDko5IJWHtuFmIu_Xg1A/edit).

This submission implements the ability to create warehouses/locations and assign inventory items to specific locations. In this case, assigning items to locations is performed by adjusting the quantity available at that location.

# Usage

This application is configured to run with [Docker compose](https://docs.docker.com/compose/), which provides an easier alternative to manually setting everything up.

If you would like to set everything up manually, see [Development environment setup](#development-environment-setup).

## Prerequisites

- [Install Docker](https://docs.docker.com/get-docker/)

  <details>

  <summary>Windows Quick-Install</summary>

  Run the following in PowerShell

  ```
  # If you already have a WSL distro, skip this.
  wsl --install -d Ubuntu
  # This part installs Docker
  winget install --exact --id Docker.DockerDesktop
  ```

  </details>

  <details>

    <summary>Linux Quick-Install</summary>

  Run the following in your POSIX-compliant shell:

  ```sh
  curl -o setup_docker.sh -L https://get.docker.com
  # Verify that you trust the contents of setup_docker.sh before running
  sudo sh setup_docker.sh
  ```

  </details>

- [Install Docker compose V2](https://docs.docker.com/compose/cli-command/#installing-compose-v2)

  Docker compose V1 should work as well, substituting `docker compose` for `docker-compose`.

## Running

Download [docker-compose.yml](docker-compose.yml) somewhere on your local machine.

In your shell, run:
```
docker compose --file <path to downloaded docker-compose.yml file> up
```

This should pull the Docker images and run the application on port 3005 of your local machine.
Please make sure that this port is available on your local machine, or edit `docker-compose.yml` to use
an available port.

Once the application is running, head to `http://localhost:3005` in your browser, and you should see the inventory items display.

From there, you can use the navigation links at the bottom to switch between the various pages and view, create, update and delete resources.

All actions available via the UI are also available as a JSON API. All endpoints are documented using brief descriptions and examples in [this postman collection](https://documenter.getpostman.com/view/5339498/UVXerxnD).

# Development environment Setup

These steps have been tested on Linux environments, though they should also work on modern Mac OS and Windows operating systems.

## Prerequisites

- NodeJS 16
- Bundler
- Ruby 3.0.0
- Repository is cloned, and your working directory is the repository root:

    ```
    git clone <repository URL> <destination folder>
    cd <destination folder>
    ```

## Steps

```bash
# Installs all the Gemfile dependencies, including Rails 7.0.
# Depending on your environment, you may need to install additional dependencies.
bundle install
yarn install
rails db:create db:migrate db:seed
rails server --port 3005
```

With this executed, you should now be able to view the website at `http://localhost:3005`, and use the HTTP JSON APIs with that base URL as well, as documented in [Running](#running).

# Improvements

- Automated testing. If we ever intend to maintain this application, automated testsare required.
- Pagination for HTTP requests - currently we return all records, but this doesn't scale.
- More attributes for inventory items. Inventory items carry a name and a per-location quantity right now. That's it.
- More filtering options. We have a few basic ones for getting inventory items by location, but it would be useful to have more (such as by timestamp, which we would support fairly easily right now).
- Use Rails in API-only mode, with a frontend in a different framework (probably React). This is what I did for the [Hunt Console](https://gitlab.com/hunt-console).

# Building the Docker Image

In the project root directory, run:
```bash
docker buildx build --tag registry.gitlab.com/kyle_anderson/shopify-tech-challenge-summer-2022:<yourtag> .
```
