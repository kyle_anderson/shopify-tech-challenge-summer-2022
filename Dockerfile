FROM ruby:3.0.0-alpine3.13 AS app 

# The base image sets this variable, however I think it's preferable not to have it.
ENV BUNDLE_APP_CONFIG=.bundle/config
# Let the dockerignore deal with the files we don't want.
COPY . /app
WORKDIR /app

RUN apk add --no-cache gcc make musl-dev g++ postgresql-dev yarn tzdata
RUN bundle config --local deployment true \
  && bundle config --local path vendor/bundle \
  && bundle install \
  && yarn install --production=true
RUN bundle exec rake assets:precompile

CMD bundle exec rails db:migrate && (bundle exec rails db:seed ; bundle exec rails server --binding 0.0.0.0 --port 80)
