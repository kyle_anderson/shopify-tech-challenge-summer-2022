locations = {:food_mart => 'Food Mart', :warehouse_one => 'Warehouse One'}
    .to_h { |k, name| [k, Location.create!(name: name)] }

items = {:apple => 'Apple', :pear => 'Pear', :blueberries => 'Blueberries'}
    .to_h { |k, name| [k, InventoryItem.create!(name: name)] }

ItemStock.create!(location: locations[:food_mart], inventory_item: items[:apple], quantity: 33)
ItemStock.create!(location: locations[:warehouse_one], inventory_item: items[:apple], quantity: 9)
ItemStock.create!(location: locations[:warehouse_one], inventory_item: items[:pear], quantity: 109)
