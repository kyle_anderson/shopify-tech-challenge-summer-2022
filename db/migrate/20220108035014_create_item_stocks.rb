class CreateItemStocks < ActiveRecord::Migration[6.1]
  def change
    create_table :item_stocks do |t|
      t.integer :quantity, null: false, default: 0
      t.references :location, null: false, foreign_key: true
      t.references :inventory_item, null: false, foreign_key: true
      t.index [:location_id, :inventory_item_id], unique: true

      t.timestamps
    end
  end
end
