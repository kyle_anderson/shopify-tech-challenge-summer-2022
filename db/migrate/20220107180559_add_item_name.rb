class AddItemName < ActiveRecord::Migration[6.1]
  def change
    add_column :inventory_items, :name, :string, null: false
  end
end
