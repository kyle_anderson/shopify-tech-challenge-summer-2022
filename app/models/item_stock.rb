# Ties an inventory item to a location, with a quantity present at that location.
class ItemStock < ApplicationRecord
  belongs_to :location
  belongs_to :inventory_item

  validates :quantity, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
