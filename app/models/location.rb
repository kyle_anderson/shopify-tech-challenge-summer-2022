class Location < ApplicationRecord
    has_many :item_stocks, dependent: :destroy
    accepts_nested_attributes_for :item_stocks

    validates :name, presence: true

    # Gets the number of different items that are in stock at this location
    def num_unique_items
        self.item_stocks.where('quantity > 0').count
    end
end
