class InventoryItem < ApplicationRecord
  # We'll allow items not to have a location set.
  has_many :item_stocks, dependent: :destroy
  accepts_nested_attributes_for :item_stocks

  validates :name, presence: true

  # Determines the total stock quantity for this item.
  def total_quantity
    item_stocks.sum(:quantity)
  end

  # Gets the number of locations that this item is available at.
  def num_locations_available
    item_stocks.where('quantity > 0').count
  end

  # Gets an ItemStock object for each location, regardless if one actually exists.
  def all_stocks
    item_stocks.select([
      'item_stocks.id',
      'l.id AS location_id',
      'CASE WHEN item_stocks.quantity IS NULL THEN 0 ELSE item_stocks.quantity END AS quantity',
      'item_stocks.created_at',
      'item_stocks.updated_at',
    ])
      .from(ActiveRecord::Base.sanitize_sql_array(["(SELECT * FROM item_stocks WHERE inventory_item_id = ?) AS item_stocks", self.id]))
      .joins('RIGHT OUTER JOIN locations AS l ON location_id = l.id OR location_id IS NULL')
      # Remove the default where clause, which filters by inventory_item_id which we've already done
      .unscope(:where)
  end
end
