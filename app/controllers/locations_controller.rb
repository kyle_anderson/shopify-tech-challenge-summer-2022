class LocationsController < ApplicationController
  def index
    respond_to do |format|
      format.html { @locations = Location.all }
      format.json { render json: Location.all }
    end
  end

  def create
    location = Location.new(location_params)
    if location.save
        respond_to do |format|
            format.html { redirect_to action: :index }
            format.json { render json: location, status: :created }
        end
    else
        render plain: location.errors, status: :unprocessable_entity
    end
  end

  def new
    @location = Location.new
  end

  def edit
    @location = current_location
  end

  def update
    current_location.update!(location_params)
    respond_to do |format|
      format.html { redirect_to action: :index }
      format.json { render status: :ok, json: current_location }
    end
  rescue StandardError => e
    render status: :internal_server_error, plain: e
  end

  def destroy
    current_location.destroy!
    respond_to do |format|
        format.html { redirect_to action: :index }
        format.json { head :ok }
    end
  end

  private

  def location_params
    params.require(:location).permit(:name)
  end

  def current_location
    id = params[:id]
    @location ||= Location.find_by(id: id)
    render status: :not_found, plain: "Location #{id} not found" if @location.blank?
    @location
  end
end
