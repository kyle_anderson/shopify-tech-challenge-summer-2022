class ApplicationController < ActionController::Base
  # Disable CSRF protection, since we're expecting client API users.
  protect_from_forgery with: :null_session
end
