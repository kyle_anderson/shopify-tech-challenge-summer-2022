class InventoryItemsController < ApplicationController
  include ItemUtils
  before_action :current_item, only: [:edit, :show, :update, :destroy]

  def index
    respond_to do |format|
      format.html do
        @items = InventoryItem.all
      end
      format.json { render json: InventoryItem.all.to_json(methods: [:total_quantity]) }
    end
  end

  def new
    @item = InventoryItem.new
  end

  def create
    item = InventoryItem.create!(inventory_item_params)
    respond_to do |format|
      format.html { redirect_to action: :index }
      format.json { render json: item, status: :created }
    end
  rescue StandardError => validation_errs
    render plain: validation_errs.message # TODO not sure about .message
  end

  def show
    respond_to do |format|
      format.html { redirect_to action: :edit }
      format.json do
        render json: current_item.to_json(include: [:item_stocks])
      end
    end
  end

  def edit
    @item = current_item
  end

  def update
    ActiveRecord::Base.transaction do
      current_item.update!(inventory_item_params.except(:item_stocks_attributes))
      if (stocks = inventory_item_params[:item_stocks_attributes]).present?
        stocks = stocks.send(stocks.is_a?(Array) ? 'map' : 'transform_values') { |stock| { **stock, inventory_item_id: current_item.id } }
        stocks = stocks.values if stocks.is_a?(ActionController::Parameters)
        # Validations get skipped here unfortunately
        ItemStock.upsert_all(
          stocks,
          # The order of the indexed columns here must match the declaration order of the migration.
          unique_by: [:location_id, :inventory_item_id],
          update_only: [:quantity],
          record_timestamps: true,
        )
      end
    end
    respond_to do |format|
      format.html { redirect_to action: :index }
      format.json { render json: current_item.to_json(include: [:item_stocks]) }
    end
  rescue StandardError => err
    render status: :internal_server_error, plain: err
  end

  def destroy
    current_item.destroy!
    respond_to do |format|
        format.html { redirect_to action: :index }
        format.json { head :ok }
    end
  end

  private

  def inventory_item_params
    params.require(:inventory_item).permit(
      :name,
      item_stocks_attributes: [:quantity, :location_id],
    )
  end
end
