require "active_support/concern"

module ItemUtils
  extend ActiveSupport::Concern

  def current_item
    id = params[:inventory_item_id] || params[:id]
    @item ||= InventoryItem.find_by(id: id)
    render status: :not_found, plain: "Item #{id} not found" if @item.blank?
    @item
  end
end
